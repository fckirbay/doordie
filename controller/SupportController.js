const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Support = db.support;

const Op = db.Sequelize.Op;

const currentDate = new Date();

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment');

exports.get = (req, res) => {
	Support.findAll({
		where:  {
			user_id: req.userId,
			[Op.or]: [
		        {
		          'validity_date': { [Op.lte]: currentDate },
		        },
		        {
		          'validity_date': null,
		        },
		    ]
		},
		order: [['id', 'ASC']],
		attributes: ['user_id', 'message', 'owner', 'date', 'validity_date', 'is_viewed'],
		limit: 30
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.post = (req, res) => {
	Support.create({
		user_id: req.userId,
		message: req.body.message,
		owner: 1,
		is_viewed: 0
	}).then(response => {
		res.status(200).json({
			"status": 200
		})
	}).catch(err => {
		res.status(500).send("Fail! Error -> " + err);
	})
}