const db = require('../config/db.config.js');
const config = require('../config/config.js');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var request = require('request');

exports.get = (req, res) => {

	if(req.query.os == "Android") {
		var categories = "1,12,16";
	} else if(req.query.os == "iOS") {
		var categories = "11,12,16";
	} else if(req.query.os == "Mac" || req.query.os == "Windows") {
		var categories = "3,12,16,17";
	}

	request({
	  url: 'https://api.adgatemedia.com/v2/offers?aff=61555&api_key=c8fc77b81b21db2cae808b995d95dd2d&traffic=virtual_currency&incent=1&categories='+ categories +'&countries='+req.query.country,
	  json: true
	}, function(error, response, body) {
		if(error) {
			res.status(500).json({
				"error": error
			});
		} else {
			res.status(200).json({
				"data": body
			});
		}
	});
}