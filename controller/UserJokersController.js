const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Userjokers = db.userjokers;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	req.filters['user_id'] = req.userId;

	Userjokers.findOne({
		where: req.filters,
		attributes: ['double_earnings', 'pass', 'show_result', 'continue_left']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.update = (req, res) => {

	if(req.body.joker == "pass") {
		Userjokers.decrement(
			{pass: req.body.count}, 
			{ where: { user_id: req.userId } 
		}).then(result =>
		    res.status(200).json({
				"status": 200
			})
		).catch(err =>
		    res.status(500).json({
				"error": err
			})
		)
	} else if(req.body.joker == "show_result") {
		Userjokers.decrement(
			{show_result: req.body.count}, 
			{ where: { user_id: req.userId } 
		}).then(result =>
		    res.status(200).json({
				"status": 200
			})
		).catch(err =>
		    res.status(500).json({
				"error": err
			})
		)
	} else if(req.body.joker == "double_earnings") {
		Userjokers.decrement(
			{double_earnings: req.body.count}, 
			{ where: { user_id: req.userId } 
		}).then(result =>
		    res.status(200).json({
				"status": 200
			})
		).catch(err =>
		    res.status(500).json({
				"error": err
			})
		)
	} else if(req.body.joker == "continue_left") {
		Userjokers.decrement(
			{continue_left: req.body.count}, 
			{ where: { user_id: req.userId } 
		}).then(result =>
		    res.status(200).json({
				"status": 200
			})
		).catch(err =>
		    res.status(500).json({
				"error": err
			})
		)
	} else {
		res.status(500).json({
			"error": "Invalid parameters!"
		})
	}

	

}