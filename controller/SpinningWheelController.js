const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Spinningwheel = db.spinningwheel;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment');

exports.get = (req, res) => {

	req.filters['user_id'] = req.userId;
	
	Spinningwheel.findAll({
		//where: { user_id: req.userId, date: { [Op.between]: [moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss'), moment().format('YYYY-MM-DD HH:mm:ss')] } },
		where: { user_id: req.userId, date: { [Op.gte]: moment().subtract(1, 'days').toDate() } },
		attributes: ['user_id', 'prize', 'date']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.post = (req, res) => {

	//var user_id = req.body.user_id;
	var user_id = req.userId;
	var result = req.body.result;

	Spinningwheel.findOne({
		where: { user_id: req.userId, date: { [Op.gte]: moment().subtract(1, 'days').toDate() } },
		attributes: ['user_id', 'prize', 'date']
	}).then(data => {
		if(data == null) {
			Spinningwheel.create({
				user_id: user_id,
				prize: result,
				date: moment().format("YYYY-MM-DD HH:mm:ss")
			}).then(response => {
				if(result == 0) {
				
				} else if(result == 1 || result == 2 || result == 3) {
			
				} else if(result == 4) {
			
				}
				res.status(200).json({
					"status": 200
				})
			}).catch(err => {
				res.status(500).send("Fail! Error -> " + err);
			})
		} else {
			res.status(500).json({
				"error": err
			});
		}
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})

	


	
	
}