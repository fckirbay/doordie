const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Slider = db.slider;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	if(req.query.lang) {
		lang = req.query.lang.toUpperCase()
	} else {
		lang = 'EN';
	}
	Slider.findAll({
		where: { lang: lang },
		order: [['order', 'ASC']]
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}