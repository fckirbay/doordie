// 24.03.2020
Alter TABLE `users2s` MODIFY COLUMN `os` VARCHAR(10);
Alter TABLE `users2s` ADD COLUMN `country_code` VARCHAR(5);

// 26.03.2020
Alter TABLE `payment_methods` ADD COLUMN `orders` INTEGER(3);
ALTER TABLE `payment_methods` ADD `subtitle` VARCHAR(255) NULL AFTER `name`;