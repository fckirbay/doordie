/*const env = {
  database: 'doordie',
  username: 'test',
  password: 'Q1w2e3xas',
  host: 'localhost',
  dialect: 'mysql',
  pool: {
	  max: 5,
	  min: 0,
	  acquire: 30000,
	  idle: 10000
  }
};*/

const env = {
  database: 'doordie',
  username: 'root',
  password: '',
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 200,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
 
module.exports = env;
