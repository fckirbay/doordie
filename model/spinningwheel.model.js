module.exports = (sequelize, Sequelize) => {
	const Spinningwheel = sequelize.define('spinning_wheel', {
	  user_id: {
		  type: Sequelize.STRING
	  },
	  prize: {
		  type: Sequelize.STRING
	  },
	  date: {
		  type: Sequelize.STRING
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Spinningwheel;
}